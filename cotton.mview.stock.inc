<?php

function tripal_megasearch_create_stock_mview() {
  $view_name = 'tripal_megasearch_stock';
  chado_search_drop_mview($view_name);
  $schema = array(
    'table' => $view_name,
    'fields' => array(
      'stock_id' => array(
        'type' => 'int',
        'not null' => TRUE,
      ),
      'name' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'uniquename' => array (
        'type' => 'text',
      ),
      'organism' => array (
        'type' => 'varchar',
        'length' => 510
      ),
      'type' => array (
        'type' => 'varchar',
        'length' => '1024'
      ),
      'collection' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'accession' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'maternal_parent' => array (
        'type' => 'text',
      ),
      'paternal_parent' => array (
        'type' => 'text',
      ),
      'pedigree' => array(
        'type' => 'text'
      ),
      'country' => array(
        'type' => 'text'
      ),
      'eimage_data' => array(
        'type' => 'text'
      ),
      'legend' => array(
        'type' => 'text'
      ),
      'description' => array (
        'type' => 'text',
      ),
      'pub' => array (
        'type' => 'text',
      ),
      'alias' => array (
        'type' => 'text',
      ),
      'grin_id' => array(
        'type' => 'varchar',
        'length' => '255'
      ),
      'genome' => array (
        'type' => 'text',
      ),
    )
  );
  $sql = "
    SELECT DISTINCT
      S.stock_id,
      S.name,
      S.uniquename,
      O.genus || ' ' || O.species AS organism,
      (CASE WHEN V.name = 'accession' THEN (SELECT value FROM chado.stockprop WHERE type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'accession_type' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'MAIN')) AND stock_id = S.stock_id) ELSE V.name END)AS type,
      COLLECTION.name AS collection,
      DATA.accession,
      MATP.uniquename AS maternal_parent,
      PATP.uniquename AS paternal_parent,
      PEDIGREE.value AS pedigree,
      (SELECT value FROM stockprop WHERE type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'origin_country' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN')) AND stock_id = S.stock_id) AS country,
      IMG.eimage_data,
      IMG.legend,
      (SELECT string_agg(value, '|') FROM stockprop WHERE stock_id = S.stock_id AND type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'description' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))) AS description,
      (SELECT string_agg(uniquename, '|') FROM pub WHERE pub_id IN (SELECT pub_id FROM stock_pub WHERE stock_id = S.stock_id)) AS pub,
      ALIAS.value,
      GRIN.accession,
      (SELECT value FROM organismprop WHERE organism_id = S.organism_id AND type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'genome_name' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN')) LIMIT 1) AS genome
    FROM stock S
    INNER JOIN organism O ON S.organism_id = O.organism_id
    INNER JOIN cvterm V ON V.cvterm_id = S.type_id AND V.name NOT IN ('sample')
    LEFT JOIN (
          SELECT stock_id, accession
          FROM dbxref X
          INNER JOIN db ON X.db_id = db.db_id
          INNER JOIN stock_dbxref SX ON X.dbxref_id = SX.dbxref_id
        ) DATA ON DATA.stock_id = S.stock_id
    LEFT JOIN (
          SELECT name, uniquename, stock_id
          FROM stockcollection SC
          INNER JOIN stockcollection_stock SCS ON SC.stockcollection_id = SCS.stockcollection_id
        ) COLLECTION ON COLLECTION.stock_id = S.stock_id
    LEFT JOIN (
      SELECT
        MAT.stock_id,
        MAT.uniquename,
        MSR.object_id
      FROM stock MAT
      INNER JOIN stock_relationship MSR ON MAT.stock_id = MSR.subject_id
      WHERE MSR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'is_a_maternal_parent_of')
    ) MATP ON MATP.object_id = S.stock_id
    LEFT JOIN (
      SELECT
        PAT.stock_id,
        PAT.uniquename,
        PSR.object_id
      FROM stock PAT
      INNER JOIN stock_relationship PSR ON PAT.stock_id = PSR.subject_id
      WHERE PSR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'is_a_paternal_parent_of')
    ) PATP ON PATP.object_id = S.stock_id
    LEFT JOIN stockprop PEDIGREE ON PEDIGREE.stock_id = S.stock_id AND PEDIGREE.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'pedigree')
    LEFT JOIN (
      SELECT
        stock_id,
        COUNTRY.value AS country
      FROM nd_experiment NE
      INNER JOIN nd_experiment_stock NES ON NES.nd_experiment_id = NE.nd_experiment_id
      INNER JOIN nd_geolocation NG ON NG.nd_geolocation_id = NE.nd_geolocation_id
      INNER JOIN nd_geolocationprop COUNTRY ON COUNTRY.nd_geolocation_id = NE.nd_geolocation_id AND COUNTRY.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'country')
    ) GEO ON S.stock_id = GEO.stock_id
    lEFT JOIN (
      SELECT stock_id, eimage_data, LEGEND.value AS legend
      FROM stock_image SI
      INNER JOIN eimage I ON SI.eimage_id = I.eimage_id
      LEFT JOIN eimageprop LEGEND ON LEGEND.eimage_id = I.eimage_id AND LEGEND.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'legend')
    ) IMG ON IMG.stock_id = S.stock_id
    LEFT JOIN (
      SELECT stock_id, string_agg(DISTINCT value, '. ') AS value
      FROM stockprop
      WHERE type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'alias' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
      GROUP BY stock_id
    ) ALIAS ON ALIAS.stock_id = S.stock_id
    LEFT JOIN (
      SELECT accession, S.stock_id
      FROM chado.stock S
      INNER JOIN chado.stock_dbxref SD ON SD.stock_id = S.stock_id
      INNER JOIN chado.dbxref DBX ON DBX.dbxref_id = SD.dbxref_id
      INNER JOIN chado.db DB ON DB.db_id = DBX.db_id
      WHERE DB.name = 'GRIN'
    ) GRIN ON GRIN.stock_id = S.stock_id
    WHERE S.type_id NOT IN (SELECT cvterm_id FROM cvterm WHERE name IN ('sample', 'clone'))
  ";
  tripal_add_mview($view_name, 'tripal_megasearch', $schema, $sql, '', FALSE);
}