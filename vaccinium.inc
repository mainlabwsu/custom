<?php

/**
 * tripal_megasearch_data_definition()
 *
 * Return the definition of the data (mviews or tables)
 *
 * $def[$table] = array (                                                 // $table should match the table name in the database
 *   'name' => 'dataset_name',                                     // The name to show in the Data Type dropdown
 *   'field' => array (                                                        // Fields to show in both Dynamic Form Filter dropdown and Downloadable Fields checkboxes
 *     $column1 => 'label1',                                            // $column should match the column in the table
 *     $column2 => 'label2',
 *     ...
 *   )
 *   'field_link_callback' => array (                                // (Optional) Hyperlink callback function that returns a URL for fields
 *     $column1 => '$callback:$column1,$column2,...'  // $column should match the column in the table. $callback is a valid PHP callback that returns a URL for link-out. $column1, $column2, etc will be passed in to the callback as arguments
 *     $column2 => '$callback:$column1,$column2,...',
 *     ...
 *   )
 *   'form' => 'static_form_callback_function',           // (Optional) Callback for the Static Form. If this is not provided or callback does not exist, fall back to use the Dynamic Form
 *   'file' => 'custom_dir/custom_file.inc'                 // (Optional) Include specified file when searching for the callback. Accept only relative path from the module root directory. Create custom sub-directory accordingly.
 *   'fasta' => 'column_to_provide_fasta_download' // (Optional) Provide a FASTA download using specified column (usually feature_id)
 *   'count' => 'column_to_count_unique_records'   // (Optional) Column for counting unique records (e.g. stock_id)
 * )
 *
 * @return $def array
 */
function tripal_megasearch_data_definition () {
  $def = array();
  $def = tripal_megasearch_contact ($def);
  $def = tripal_megasearch_featuremap ($def);
  $def = tripal_megasearch_gene ($def);
  $def = tripal_megasearch_marker ($def);
  $def = tripal_megasearch_pub ($def);
  $def = tripal_megasearch_qtl ($def);
  $def = tripal_megasearch_stock ($def);
  $def = tripal_megasearch_ortholog ($def);
  return $def;
}

function tripal_megasearch_contact ($def = array()) {
  $def['tripal_megasearch_contact'] = array(
    'name' => 'Contact',
    'field' => array(
      'type' => 'Type',
      'name' => 'Full Name',
      'lname' => 'Last Name',
      'fname' => 'First Name',
      'institution' => 'Institution',
      'country' => 'Country',
      'research_interest' => 'Research Interest'
    ),
    'field_link_callback' => array(
      'name' => 'chado_search_link_contact:contact_id',
    ),
    'form' => 'tripal_megasearch_contact_form',
    'file' => 'includes/tripal_megasearch.form.static.contact.inc',
    'count' => 'contact_id',
    'checkbox_filter' => 'contact_id'
  );
  return $def;
}

function tripal_megasearch_featuremap ($def = array()) {
  $def['tripal_megasearch_featuremap'] = array(
    'name' => 'Map',
    'field' => array(
      'name' => 'Map Name',
      'description' => 'Description',
      'unittype' => 'Unit Type',
      'organism' => 'Organism',
      'population' => 'Population',
      'maternal_parent' => 'Maternal Parent',
      'paternal_parent' => 'Paternal Parent',
      'num_of_lg' => 'Number of LG',
      'num_of_loci' => 'Number of Loci',
      'num_of_qtl' => 'Number of QTL',
      'citation' => 'Publication'
    ),
    'field_link_callback' => array(
      'name' => 'chado_search_link_featuremap:featuremap_id',
    ),
    'form' => 'tripal_megasearch_featuremap_form',
    'file' => 'includes/tripal_megasearch.form.static.featuremap.inc',
    'mview_file' => 'custom/main.mview.marker.inc',
    'mview_file' => 'custom/main.mview.featuremap.inc',
    'count' => 'featuremap_id',
    'checkbox_filter' => 'featuremap_id'
  );
  return $def;
}

function tripal_megasearch_gene ($def = array()) {
  $def['tripal_megasearch_gene'] = array(
    'name' => 'Gene/Transcript',
    'field' => array(
      'name' => 'Name',
      'uniquename' => 'Unique Name',
      'organism' => 'Organism',
      'type' => 'Type',
      'analysis' => 'Genome/Transcriptome',
      'landmark' => 'Chromosome/Scaffold',
      'fmin' => 'Start position',
      'fmax' => 'Stop position',
      'location' => 'Location',
      'blast' => 'BLAST',
      //'kegg' => 'KEGG',
      'interpro' => 'InterPro',
      'go' => 'GO Term',
      'go_acc' => 'GO Accession',
      'genbank' => 'GenBank Keyword'
    ),
    'field_link_callback' => array(
      'uniquename' => 'chado_search_link_feature:feature_id',
      'organism' => 'chado_search_link_organism:organism_id',
      'analysis' => 'tripal_megasearch_lookup_analysis:analysis',
      'location' => 'tripal_megasearch_lookup_jbrowse:genome,location',
    ),
    'form' => 'tripal_megasearch_gene_form',
    'file' => 'includes/tripal_megasearch.form.static.gene.inc',
    'fasta' => 'feature_id',
    'count' => 'feature_id',
    'checkbox_filter' => 'feature_id',
    //'duplicates' => 'not_remove',
  );
  return $def;
}

function tripal_megasearch_marker ($def = array()) {
  $def['tripal_megasearch_marker'] = array(
    'name' => 'Marker',
    'field' => array(
      'uniquename' => 'Unique Name',
      'name' => 'Marker Name',
      'organism' => 'Organism',
      'mapped_organism' => 'Mapped Organism',
      'marker_type' => 'Marker Type',
      'residues' => 'Sequence',
      'map' => 'Map',
      'lg' => 'Linkage Group',
      'start' => 'Map Position (Start)',
      'stop' => 'Map Position (Stop)',
      'genome' => 'Genome',
      'landmark' => 'Landmark',
      'fmin' => 'Genome Position (Start)',
      'fmax' => 'Genome Position (Stop)',
      'location' => 'Location',
      'alias' => 'Alias',
      'synonym' => 'Synonym',
      'trait' => 'Trait',
      'snp_array' => 'SNP Array',
      'array_id' => 'Array ID',
      'dbsnp_id' => 'dbSNP ID',
      'allele' => 'Allele',
      'primers' => 'Primers',
      'probes' => 'Probes',
      'pubs' => 'Publications',
    ),
    'field_link_callback' => array(
      'uniquename' => 'chado_search_link_feature:feature_id',
      'organism' => 'chado_search_link_organism:organism_id',
      'mapped_organism' => 'tripal_megasearch_lookup_organism:mapped_organism',
      'map' => 'tripal_megasearch_lookup_featuremap:map',
      'genome' => 'tripal_megasearch_lookup_analysis:genome',
      'location' => 'tripal_megasearch_lookup_jbrowse:genome,location',
    ),
    'form' => 'tripal_megasearch_marker_form',
    'file' => 'includes/tripal_megasearch.form.static.marker.inc',
    'mview_file' => 'custom/main.mview.marker.inc',
    'count' => 'feature_id',
    'checkbox_filter' => 'feature_id',
    //'duplicates' => 'not_remove',
  );
  return $def;
}

function tripal_megasearch_pub ($def = array()) {
  $def['tripal_megasearch_pub'] = array(
    'name' => 'Publication',
    'field' => array(
      'title' => 'Title',
      'volume' => 'Volume',
      'series_name' => 'Journal Name',
      'issue' => 'Issue',
      'pyear' => 'Year',
      'pages' => 'Pages',
      'citation' => 'Citation',
      'type' => 'Publication Type',
      'authors' => 'Authors',
      'abstract' => 'Abstract'
    ),
    'field_link_callback' => array(
      'title' => 'chado_search_link_pub:pub_id',
    ),
    'form' => 'tripal_megasearch_pub_form',
    'file' => 'includes/tripal_megasearch.form.static.pub.inc',
    'count' => 'pub_id',
    'checkbox_filter' => 'pub_id'
  );
  return $def;
}

function tripal_megasearch_qtl ($def = array()) {
  $def['tripal_megasearch_qtl'] = array(
    'name' => 'QTL/GWAS',
    'field' => array(
      'qtl' => 'QTL/GWAS Label',
      'trait' => 'Trait Name',
      'category' => 'Trait Category',
      'gwas_marker_uniquename' => 'GWAS Marker',
      'gwas_gene_uniquename' => 'Gene',
      'organism' => 'Organism',
      'type' => 'Type',
      'project' => 'Dataset',
      'symbol' => 'Published Symbol',
      'p_value' => 'P value',
      'lod' => 'LOD',
      'r2' => 'R2',
      'genome' => 'Genome',
      'location' => 'Location',
      'map' => 'Map',
      'lg' => 'Linkage Group',
      'start' => 'Start (cM)',
      'stop' => 'Stop (cM)',
      'qtl_peak' => 'QTL Peak',
      'col_marker_uniquename' => 'Colocalizing Marker',
      'neighbor_marker_uniquename' => 'Neighboring Marker',
      'population' => 'Population/GWAS panel',
      'maternal_parent' => 'Maternal Parent',
      'paternal_parent' => 'Paternal Parent',
      'citation' => 'Citation',
    ),
    'field_link_callback' => array(
      'qtl' => 'chado_search_link_feature:feature_id',
      'trait' => 'tripal_megasearch_qtl_link_trait:cvterm_id',
      'col_marker_uniquename' => 'chado_search_link_feature:c_m_feature_id',
      'neighbor_marker_uniquename' => 'chado_search_link_feature:n_m_feature_id',
      'gwas_marker_uniquename' => 'chado_search_link_feature:g_m_feature_id',
      'gwas_gene_uniquename' => 'chado_search_link_feature:g_g_feature_id',
      'project' => 'chado_search_link_project:project_id',
      'organism' => 'chado_search_link_organism:organism_id',
      'map' => 'tripal_megasearch_lookup_featuremap:map',
      'population' => 'tripal_megasearch_lookup_stock:population',
      'citation' => 'tripal_megasearch_lookup_pub:citation',
      'genome' => 'chado_search_link_analysis:genome_id',
      'location' => 'tripal_megasearch_lookup_jbrowse:genome,location',
    ),
    'form' => 'tripal_megasearch_qtl_form',
    'file' => 'custom/main.form.static.qtl.inc',
    'mview_file' => 'custom/main.mview.qtl.inc',
    'count' => 'feature_id',
    'checkbox_filter' => 'feature_id'
  );
  return $def;
}

function tripal_megasearch_stock ($def = array()) {
  $def['tripal_megasearch_stock'] = array(
    'name' => 'Germplasm',
    'field' => array(
      'name' => 'Name',
      'uniquename' => 'Unique Name',
      'organism' => 'Organism',
      //'collection' => 'Collection',
      'accession' => 'Accession',
      'maternal_parent' => 'Maternal Parent',
      'paternal_parent' => 'Paternal Parent',
      'pedigree' => 'Pedigree',
      //'country' => 'Country',
      //'eimage_data' => 'Image Name',
      //'legend' => 'Legend',
      'pub' => 'Publication'
    ),
    'field_link_callback' => array(
      'uniquename' => 'chado_search_link_stock:stock_id',
      'maternal_parent' => 'tripal_megasearch_lookup_stock:maternal_parent',
      'paternal_parent' => 'tripal_megasearch_lookup_stock:paternal_parent',
    ),
    'form' => 'tripal_megasearch_stock_form',
    'file' => 'custom/vaccinium.form.static.stock.inc',
    'mview_file' => 'custom/vaccinium.mview.stock.inc',
    'count' => 'stock_id',
    'checkbox_filter' => 'stock_id'
  );
  return $def;
}

function tripal_megasearch_ortholog ($def = array()) {
  $def['tripal_megasearch_ortholog'] = array(
    'name' => 'Ortholog/Paralog',
    'field' => array(
      'l_genome' => 'Genome1',
      'l_landmark' => 'Chromosome1',
      'l_location' => 'Location1',
      'l_feature' => 'Ortholog1',
      'r_genome' => 'Genome2',
      'r_landmark' => 'Chromosome2',
      'r_location' => 'Location2',
      'r_feature' => 'Ortholog2',
      'associated_gene' => 'Associated Gene'
    ),
    'field_link_callback' => array(
      'l_genome' => 'chado_search_link_analysis:l_analysis_id',
      'l_feature' => 'chado_search_link_feature:l_feature_id',
      'r_genome' => 'chado_search_link_analysis:r_analysis_id',
      'r_feature' => 'chado_search_link_feature:r_feature_id',
      'associated_gene' => 'chado_search_link_feature:associated_feature_id',
    ),
    'form' => 'tripal_megasearch_ortholog_form',
    'file' => 'includes/tripal_megasearch.form.static.ortholog.inc',
    'count' => 'r_feature_id',
    'checkbox_filter' => 'l_feature_id'
  );
  return $def;
}