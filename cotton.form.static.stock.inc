<?php

use ChadoSearch\Set;
use ChadoSearch\Sql;

function tripal_megasearch_stock_form ($form) {
  $form_state =& $form->form_state;

  // Settings for the Filter & Attribute lists (hide if datatype not selected)
  $setttings_filter =
  Set::dynamicFieldset()
  ->id('data_filters')
  ->title('Query')
  ->dependOnId('datatype');
  $setttings_attr =
  Set::dynamicFieldset()
  ->id('data_attributes')
  ->title('Downloadable Fields')
  ->dependOnId('datatype');
  if(!isset($form_state['values']['datatype']) || $form_state['values']['datatype'] == '0') {
    $setttings_filter->display('none');
    $setttings_attr->display('none');
  }

  // Filter List fieldset
  $form->addDynamicFieldset($setttings_filter);

  // Attribute List fieldset
  $form->addDynamicFieldset($setttings_attr);

  if(isset($form_state['values']['datatype'])) {
    $mview = $form_state['values']['datatype'];
    $fields = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'field');
    if ($fields) {
      $form->addButton(
          Set::button()
          ->id('tsv-download')
          ->value('TSV')
          ->fieldset('data_attributes')
          ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_stock_create_download',
              'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
              'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_progress_attribute())
          );
      $form->addSubmit(
          Set::submit()
          ->value('CSV')
          ->fieldset('data_attributes')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_stock_create_download',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_progress_attribute())
          );
      $default_rows = variable_get('tripal_megasearch_result_rows', '10');
      if ($default_rows > 0) {
        $form->addButton(
            Set::button()
            ->id('view_results')
            ->fieldset('data_attributes')
            ->value('View')
            ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_stock_view_result',
              'wrapper' => 'chado_search-tripal_megasearch-markup-results',
              'effect' => 'fade')
                )
            );
      }
      $form->addButton(
          Set::button()
          ->id('apply_filters')
          ->fieldset('data_filters')
          ->value('Refresh Count')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_stock_refresh_count',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          );
      $form->addButton(
          Set::button()
          ->id('clear-all-values')
          ->fieldset('data_filters')
          ->value('Clear')
          ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_filter_clear_values',
              'wrapper' => 'chado_search-filter-tripal_megasearch-data_filters-field',
              'effect' => 'fade')
              )
          );

      // Name
      $form->addTextFilter(
          Set::textFilter()
          ->id('stock_name')
          ->title('Name')
          ->labelWidth(140)
          ->fieldset('data_filters')
          );
      $form->addFile(
          Set::file()
          ->id('stock_file')
          ->title("File Upload")
          ->fieldset('data_filters')
          ->description("Provide germplasm names in a file. Separate each name by a new line.")
          );
      // Organism
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('stock_organism')
          ->title('Species')
          ->table('tripal_megasearch_stock')
          ->column('organism')
          ->multiple(TRUE)
          ->searchBox(TRUE)
          ->fieldset('data_filters')
          ->labelWidth(140)
          ->cache(TRUE)
          ->newline()
          );
      // Type
      $form->addSelectFilter(
        Set::selectFilter()
        ->id('stock_type')
        ->title('Type')
        ->table('tripal_megasearch_stock')
        ->column('type')
        ->fieldset('data_filters')
        ->labelWidth(140)
        ->cache(TRUE)
        ->newline()
        );
      $form->addLabeledFilter(
          Set::labeledFilter()
          ->id('stock_description')
          ->title('Keywords')
          ->fieldset('data_filters')
          );
       $form->addMarkup(
          Set::markup()
          ->id('stock_description_example')
           ->fieldset('data_filters')
          ->text('Provide words in germplasm description (e.g. wilt, resist, mutant, toleran, maturity)')
          ->newLine()
          );
       $form->addFieldset(
          Set::fieldset()
          ->id('stock_group_name')
          ->title('Germplasm name')
          ->startWidget('stock_name')
          ->endWidget('stock_description_example')
          );

      // Collection
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('stock_collection')
          ->title('Collection')
          ->table('tripal_megasearch_stock')
          ->column('collection')
          ->labelWidth(140)
          ->cache(TRUE)
          ->fieldset('data_filters')
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('stock_accession')
          ->title('Accession')
          ->labelWidth(140)
          ->fieldset('data_filters')
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('stock_group_collection')
          ->title('Collection')
          ->startWidget('stock_collection')
          ->endWidget('stock_accession')
          );
      // Pedigree
      $form->addTextFilter(
          Set::textFilter()
          ->id('stock_pedigree')
          ->title('Pedigree')
          ->labelWidth(140)
          ->fieldset('data_filters')
          );
      $form->addMarkup(
          Set::markup()
          ->id('stock_pedigree_example')
          ->text('(e.g.: DP*90)')
          ->fieldset('data_filters')
          ->newLine()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('stock_group_parentage')
          ->title('Pedigree')
          ->description('Search germplasm by pedigree. Wild card (*) can be used to match any word.')
          ->startWidget('stock_pedigree')
          ->endWidget('stock_pedigree_example')
          );

      // Country
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('stock_country')
          ->title('Country')
          ->table('tripal_megasearch_stock')
          ->column('country')
          ->labelWidth(140)
          ->fieldset('data_filters')
          ->cache(TRUE)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('stock_group_country')
          ->title('Country')
          ->startWidget('stock_country')
          ->endWidget('stock_country')
          );

      // Image
      $form->addTextFilter(
          Set::textFilter()
          ->id('stock_eimage_data')
          ->title('Name')
          ->labelWidth(140)
          ->fieldset('data_filters')
          );
      $form->addMarkup(
        Set::markup()
        ->id('stock_name_example')
        ->fieldset('data_filters')
        ->text('(e.g. tm1)')
        ->newLine()
        );
      $form->addTextFilter(
          Set::textFilter()
          ->id('stock_legend')
          ->title('Legend')
          ->labelWidth(140)
          ->fieldset('data_filters')
          );
      $form->addMarkup(
        Set::markup()
        ->id('stock_legend_example')
        ->fieldset('data_filters')
        ->text('(e.g. flower, boll open, A2-, GB-)')
        ->newLine()
        );
      $form->addFieldset(
          Set::fieldset()
          ->id('stock_group_image')
          ->title('Image')
          ->startWidget('stock_eimage_data')
          ->endWidget('stock_legend_example')
          );

      // Select/unselect all fields
      $form->addCheckBoxes(
          Set::checkboxes()
          ->id('attribute_check_all')
          ->options(
              array(
                'select_all' => 'All Fields',
              )
              )
          ->ajax(
              array(
                'callback' => 'tripal_megasearch_ajax_toggle_field_selection',
                'wrapper' => 'chado_search-tripal_megasearch-checkboxes-attribute_checkboxes',
                'effect' => 'fade'
              )
              )
          ->defaultValue(array('select_all'))
          ->fieldset('data_attributes')
          );
      // Populate attribuet list
      $form->addCheckboxes(
          Set::checkBoxes()
          ->id('attribute_checkboxes')
          ->options($fields)
          ->fieldset('data_attributes')
          ->defaultValue(array_keys($fields))
          );
    }
  }
  return $form;
}

function tripal_megasearch_ajax_stock_get_filters(&$form_state) {
  $where = array();
  $where [] = Sql::selectFilter('stock_organism', $form_state, 'organism');
  $where [] = Sql::selectFilter('stock_type', $form_state, 'type');
  $where [] = Sql::textFilterOnMultipleColumns('stock_name', $form_state, array('uniquename', 'name', 'alias'));
  $where [] = Sql::fileOnMultipleColumns('stock_file', array('uniquename', 'name', 'alias'), FALSE, TRUE);
  $where [] = Sql::selectFilter('stock_collection', $form_state, 'collection');
  $where [] = Sql::textFilter('stock_accession', $form_state, 'accession');
  if ($form_state['values']['stock_pedigree'] != 'exactly') {
    $where [] = str_replace('*', '%', Sql::textFilter('stock_pedigree', $form_state, 'pedigree'));
  }
  else {
    $where [] = Sql::textFilter('stock_pedigree', $form_state, 'pedigree');
  }
  $where [] = Sql::selectFilter('stock_country', $form_state, 'country');
  $where [] = Sql::textFilter('stock_eimage_data', $form_state, 'eimage_data');
  $where [] = Sql::textFilter('stock_legend', $form_state, 'legend');
  $where [] = Sql::labeledFilter('stock_description', $form_state, 'description', FALSE, 'contains');
  return $where;
}

/*
 * Refresh count
 */
function tripal_megasearch_ajax_stock_refresh_count ($form, &$form_state) {
  $where = tripal_megasearch_ajax_stock_get_filters($form_state);
  return tripal_megasearch_get_form_status($form, $form_state, $where);
}

function tripal_megasearch_ajax_stock_view_result ($form, &$form_state) {
  $where = tripal_megasearch_ajax_stock_get_filters($form_state);
  $result = $form['results'];
  $result['#markup'] = tripal_megasearch_ajax_get_table ($form, $form_state, $where);
  return $result;
}

/*
  * Create download
  */
function tripal_megasearch_ajax_stock_create_download ($form, &$form_state) {
  $where = tripal_megasearch_ajax_stock_get_filters($form_state);
  $format = $form_state['triggering_element']['#value'] == 'TSV' ? 'TSV' : 'CSV';
  return tripal_megasearch_ajax_get_download_file ($form, $form_state, $where, $format);
}

function tripal_megasearch_stock_link_grin ($grin_id) {
  if ($grin_id) {
    return "https://npgsweb.ars-grin.gov/gringlobal/accessiondetail?accid=$grin_id";
  }
  else {
    return NULL;
  }
}