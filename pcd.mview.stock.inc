<?php

function tripal_megasearch_create_stock_mview() {
  $view_name = 'tripal_megasearch_stock';
  chado_search_drop_mview($view_name);
  $schema = array(
    'table' => $view_name,
    'fields' => array(
      'stock_id' => array(
        'type' => 'int',
        'not null' => TRUE,
      ),
      'name' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'uniquename' => array (
        'type' => 'text',
      ),
      'alias' => array (
        'type' => 'text',
      ),
      'analysis_name' => array (
        'type' => 'text',
      ),
      'cultivar' => array (
        'type' => 'text',
      ),
      'institution' => array (
        'type' => 'text',
      ),
      'organism' => array (
        'type' => 'varchar',
        'length' => 510
      ),
      'type' => array (
        'type' => 'varchar',
        'length' => '1024'
      ),
      'collection' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'accession' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'maternal_parent' => array (
        'type' => 'text',
      ),
      'paternal_parent' => array (
        'type' => 'text',
      ),
      'pedigree' => array(
        'type' => 'text'
      ),
      'country' => array(
        'type' => 'text'
      ),
      'eimage_data' => array(
        'type' => 'text'
      ),
      'legend' => array(
        'type' => 'text'
      ),
      'description' => array (
        'type' => 'text',
      ),
      'pub_id' => array(
        'type' => 'int',
      ),
      'pub' => array (
        'type' => 'text',
      ),
      'project_id' => array(
        'type' => 'int',
      ),
      'dataset' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'project_type' => array (
        'type' => 'text'
      ),
    )
  );
  $sql = "
    SELECT DISTINCT
      S.stock_id,
      S.name,
      S.uniquename,
      ALIAS.value AS alias,
      ANAME.value AS analysis_name,
      CULTIVAR.value AS cultivar,
      INST.value AS institution,
      O.genus || ' ' || O.species AS organism,
      V.name AS type,
      COLLECTION.name AS collection,
      DATA.accession,
      MATP.uniquename AS maternal_parent,
      PATP.uniquename AS paternal_parent,
      PEDIGREE.value AS pedigree,
      GEO.country,
      IMG.eimage_data,
      IMG.legend,
      (SELECT string_agg(value, '|') FROM stockprop WHERE stock_id = S.stock_id AND type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'description' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))) AS description,
      PUB.pub_id,
      PUB.uniquename AS pub,
      PROJ.project_id,
      PROJ.name AS dataset,
      (SELECT value FROM projectprop WHERE project_id = PROJ.project_id AND type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'project_type' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))) AS project_type
    FROM stock S
    INNER JOIN organism O ON S.organism_id = O.organism_id
    INNER JOIN cvterm V ON V.cvterm_id = S.type_id AND V.name IN ('accession', 'panel', 'population')
    LEFT JOIN (
          SELECT stock_id, accession
          FROM dbxref X
          INNER JOIN db ON X.db_id = db.db_id
          INNER JOIN stock_dbxref SX ON X.dbxref_id = SX.dbxref_id
        ) DATA ON DATA.stock_id = S.stock_id
    LEFT JOIN (
          SELECT name, uniquename, stock_id
          FROM stockcollection SC
          INNER JOIN stockcollection_stock SCS ON SC.stockcollection_id = SCS.stockcollection_id
        ) COLLECTION ON COLLECTION.stock_id = S.stock_id
    LEFT JOIN (
      SELECT
        MAT.stock_id,
        MAT.uniquename,
        MSR.object_id
      FROM stock MAT
      INNER JOIN stock_relationship MSR ON MAT.stock_id = MSR.subject_id
      WHERE MSR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'is_a_maternal_parent_of')
    ) MATP ON MATP.object_id = S.stock_id
    LEFT JOIN (
      SELECT
        PAT.stock_id,
        PAT.uniquename,
        PSR.object_id
      FROM stock PAT
      INNER JOIN stock_relationship PSR ON PAT.stock_id = PSR.subject_id
      WHERE PSR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'is_a_paternal_parent_of')
    ) PATP ON PATP.object_id = S.stock_id
    LEFT JOIN stockprop PEDIGREE ON PEDIGREE.stock_id = S.stock_id AND PEDIGREE.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'pedigree')
    LEFT JOIN stockprop ALIAS ON ALIAS.stock_id = S.stock_id AND ALIAS.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'alias' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
    LEFT JOIN stockprop ANAME ON ANAME.stock_id = S.stock_id AND ANAME.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'analysis_name' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
    LEFT JOIN stockprop CULTIVAR ON CULTIVAR.stock_id = S.stock_id AND CULTIVAR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'cultivar' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
    LEFT JOIN stockprop INST ON PEDIGREE.stock_id = S.stock_id AND INST.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'institution' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
    LEFT JOIN (
      SELECT
        stock_id,
        COUNTRY.value AS country
      FROM nd_experiment NE
      INNER JOIN nd_experiment_stock NES ON NES.nd_experiment_id = NE.nd_experiment_id
      INNER JOIN nd_geolocation NG ON NG.nd_geolocation_id = NE.nd_geolocation_id
      INNER JOIN nd_geolocationprop COUNTRY ON COUNTRY.nd_geolocation_id = NE.nd_geolocation_id AND COUNTRY.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'country')
    ) GEO ON S.stock_id = GEO.stock_id
    LEFT JOIN (
      SELECT stock_id, eimage_data, LEGEND.value AS legend
      FROM stock_image SI
      INNER JOIN eimage I ON SI.eimage_id = I.eimage_id
      LEFT JOIN eimageprop LEGEND ON LEGEND.eimage_id = I.eimage_id AND LEGEND.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'legend')
    ) IMG ON IMG.stock_id = S.stock_id
    LEFT JOIN (SELECT DISTINCT project_id, stock_id FROM nd_experiment_project NEP INNER JOIN nd_experiment_stock NES ON NEP.nd_experiment_id = NES.nd_experiment_id UNION SELECT DISTINCT project_id, stock_id FROM genotype_call) GTYPE ON GTYPE.stock_id = S.stock_id
    LEFT JOIN project PROJ ON PROJ.project_id = GTYPE.project_id
    LEFT JOIN stock_pub SPUB ON SPUB.stock_id = S.stock_id
    LEFT JOIN project_pub PJPUB ON PJPUB.project_id = PROJ.project_id
    LEFT JOIN pub PUB ON PUB.pub_id = PJPUB.pub_id OR PUB.pub_id = SPUB.pub_id
  ";
  tripal_add_mview($view_name, 'tripal_megasearch', $schema, $sql, '', FALSE);
}